# Prerequisites
* Web Development Environment
    * Apache
    * MySQL
    * PHP >= 7.1
* Git ([Install Instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git))
* Composer ([Install Instructions](https://getcomposer.org/doc/00-intro.md#installation-windows))

# Installation
1. Clone this project: `git clone git@gitlab.com:ocgcompanies/developer_test.git`
2. Create a Virtual Host for local development for this project
3. `cd developer_test`
4. Install Composer dependencies: `composer install`
5. Make a copy of the `.env.sample` file and name it `.env`
6. Edit database credentials in the new `.env` file
7. Run the database migrations / seeds
    * Migrations and Seeds use [Phinx](http://docs.phinx.org/en/latest/intro.html)
    * From project root, run:
        * `./vendor/bin/phinx migrate`
        * `./vendor/bin/phinx seed:run`


# Goal
1. Load Address information from the database
2. Display the Address information to the user on the home page

#### Questions?
If at any point you have any questions, please feel free to ask.  
[Jake White (jwhite@ocgcompanies.com)](mailto:jwhite@ocgcompanies.com)  
[Jason Simpson (jsimpson@ocgcompanies.com)](mailto:jsimpson@ocgcompanies.com)  
[Bruce Sheridan (bsheridan@ocgcompanies.com)](mailto:bsheridan@ocgcompanies.com)


##### Hints:
* A couple classes have been started already in the namespace `OCGCompanies\Domain\Address`.
* There's a helper function available to get variables from the `.env` file after `bootstrap.php` has been included. e.g. `getenv('database.host')`