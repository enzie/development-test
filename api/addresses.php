<?php
namespace OCGCompanies;

$app = require_once '../bootstrap.php';

$addressDAO = new Domain\Address\AddressDAO($app['database.pdo']);
$addresses = $addressDAO->findAll();

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

echo json_encode($addresses);
