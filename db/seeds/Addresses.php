<?php


use Phinx\Seed\AbstractSeed;

class Addresses extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * a_address
     * a_street_name
     * a_city
     * a_state
     * a_zip
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 100; $i++) {
            $data[] = [
                'a_address' => $faker->buildingNumber,
                'a_street_name' => $faker->streetName,
                'a_city' => $faker->city,
                'a_state' => $faker->stateAbbr,
                'a_zip' => $faker->postcode,
            ];
        }

        $this->table('addresses')->insert($data)->save();
    }
}
