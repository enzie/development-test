<?php


use Phinx\Migration\AbstractMigration;

class Address extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other distructive changes will result in an error when trying to
     * rollback the migration.
     */
    public function change()
    {
        $this->table('addresses', ['id' => 'a_id'])
            ->addColumn('a_address', 'integer')
            ->addColumn('a_street_name', 'string', ['limit' => 32])
            ->addColumn('a_city', 'string', ['limit' => 32])
            ->addColumn('a_state', 'string', ['limit' => 2])
            ->addColumn('a_zip', 'string', ['limit' => 5])
            ->create();
    }
}
