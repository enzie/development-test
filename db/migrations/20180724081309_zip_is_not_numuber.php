phinxlog<?php

use Phinx\Migration\AbstractMigration;

class ZipIsNotNumuber extends AbstractMigration
{
    /*
     * A zip code, while it looks like number, is not a number. It is a unique identifier that just
     * happens to be numeric. Storing them as a number can cause problems:
     *  - Leading zeros will get truncated.
     *  - Extended zips will not work. $faker->postcode returns extended zips on occasaion.
     *  - Data model locked to USA system.
     */
    public function change()
    {
        $this->table('addresses')
            ->removeColumn('a_zip')
            ->addColumn('a_zip', 'string', ['limit' => 20])
            ->save();
    }
}
