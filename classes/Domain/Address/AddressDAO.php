<?php

namespace OCGCompanies\Domain\Address;


class AddressDAO
{
    /* @var \PDO $pdo */
    private $pdo = null;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return Address[]
     */
    public function findAll(): array
    {
        try {
            $statement = $this->pdo->prepare("
                SELECT * FROM addresses;
            ");
            $statement->execute();
            return $this->mapRowsToDomainObjects($statement);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param \PDOStatement $statement
     * @return Address[]
     */
    private function mapRowsToDomainObjects(\PDOStatement $statement): array
    {
        $addresses = [];
        try {
            if( ($row = $statement->fetch(\PDO::FETCH_ASSOC)) === false ) {
                throw new \Exception('No Records Found');
            }
            do {
                $addresses[] = $this->mapRowToDomainObject($row);
            } while( ($row = $statement->fetch(\PDO::FETCH_ASSOC)) !== false );
        } catch( \PDOException $e ) {
            throw new \Exception($e->getMessage(), (int)$e->getCode(), $e);
        }
        return $addresses;
    }

    private function mapRowToDomainObject(array $row): Address
    {
        $address = new Address;
        $address->setId($row['a_id']);
        $address->setAddress($row['a_address']);
        $address->setStreetName($row['a_street_name']);
        $address->setCity($row['a_city']);
        $address->setState($row['a_state']);
        $address->setZip($row['a_zip']);

        return $address;
    }
}