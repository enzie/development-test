<!DOCTYPE html>
<html>
<head>
    <title>OCG Developer Test</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css" />
    <script type="text/javascript" src="/assets/js/vue.js" defer></script>
    <script type="text/javascript" src="/assets/js/jquery-3.3.1.min.js" defer></script>
    <script type="text/javascript" src="/assets/js/axios.min.js" defer></script>
    <script type="text/javascript" src="/assets/js/app.js" defer></script>
</head>

<body>
<div id="app">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="/">OCG Companies</a>
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#nav-bar-content"
                aria-controls="nav-bar-content"
                aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="nav-bar-content">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">OCG Companies</h1>
            <p class="lead">This is a simple PHP development test</p>
        </div>

            <div class="card bg-light mb-3">
            
                <div class="card-header">Address Information</div>
                <div class="card-body">                
                <div class="columns medium-4">
                    <div class="card">
                        <div class="card-section">
                            <address-list></address-list>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
