<?php

use Dotenv\Dotenv;

require 'vendor/autoload.php';

$env = new Dotenv(__DIR__);
$env->load();

$app['database.host'] = getenv('database.host');
$app['database.user'] = getenv('database.user');
$app['database.pass'] = getenv('database.pass');
$app['database.name'] = getenv('database.name');

$dsn = sprintf('mysql:host=%s;dbname=%s', $app['database.host'], $app['database.name']);

$app['database.pdo'] = new \PDO($dsn, $app['database.user'], $app['database.pass']);

return $app;
