<?php

require 'bootstrap.php';

return [
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'main',
        'main' => [
            'name' => getenv('database.name'),
            'adapter' => 'mysql',
            'host' => getenv('database.host'),
            'user' => getenv('database.user'),
            'pass' => getenv('database.pass'),
            'charset' => 'utf8',
        ],
    ],
    'paths' => [
        'migrations' => 'db/migrations',
        'seeds' => 'db/seeds',
    ],
    'version_order' => 'execution',
];