Vue.component("address-list", {
  data: function() {
    return {
      results: []
    };
  },
  mounted: function() {
    const url = "/api/addresses.php";
    axios.get(url).then(response => {
      console.log(response);
      this.results = response.data;
    });
  },
  template: `
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Address</th>
          <th scope="col">Street Name</th>
          <th scope="col">City</th>
          <th scope="col">State</th>
          <th scope="col">Zip</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(result, index) in results" v-cloak>
          <th scope="row">{{ result.id }}</th>
          <td>{{ result.address }}</td>
          <td>{{ result.street_name }}</td>
          <td>{{ result.city }}</td>
          <td>{{ result.state }}</td>
          <td>{{ result.zip }}</td>
        </tr>
      </tbody>
    </table>
  `
})

const vm = new Vue({
  el: "#app"
})